<?php
$data = [
        [
            'nombre' => 'Coca Cola',
            'cant' => '100',
            'precio' => 4500 ,
        ],
         [
            'nombre' => 'Pepsi',
            'cant' => '30',
            'precio' => 4800,
        ],
         [
            'nombre' => 'Sprite',
            'cant' => '20',
            'precio' => 4500,
        ],
         [
            'nombre' => 'Guaraná',
            'cant' => '200',
            'precio' => 4500,
        ],
         [
            'nombre' => 'SevenUp',
            'cant' => '24',
            'precio' => 4800,
        ],
         [
            'nombre' => 'Mirinda Naranja',
            'cant' => '56',
            'precio' => 4800,
        ],
         [
            'nombre' => 'Mirinda Guaraná',
            'cant' => '89',
            'precio' => 4800,
        ],
         [
            'nombre' => 'Fanta Naranja',
            'cant' => '10',
            'precio' => 4500,
        ],
         [
            'nombre' => 'Fanta Piña',
            'cant' => '2',
            'precio' => 4500,
        ]
    ];
?>

<!DOCTYPE html>
<html lang="en">
 	<head>
    <style>
    caption {
     background-color: yellow;
    }  
  
    th{
      background-color: #cdcdcd;

    }
      
    tr:nth-child(odd) { 
      background-color: #d5f8d5;
     
    }
	 table, td,th{
        border:1px solid black ;
        border-collapse:collapse;
      }
      
   
    </style>
    </head>
    <body>
       <table >
       <caption> Productos </caption>
            <tr>
                <th  style="font-weight: normal;"> Nombre</th>
                <th  style="font-weight: normal;"  width= 80px>Cantidad</th>
                <th  style="font-weight: normal;" width= 65px >Precio</th>
            </tr>

            <?php

                $concat = '';

                foreach ($data as $producto) {

 
                    $concat .= '<tr>';
                    $concat .= '<td>' . $producto['nombre'] .'</td>';
                    $concat .= '<td>' . $producto['cant'] .'</td>';

                 
                    $concat .= '<td>' . number_format($producto['precio'], 0, '.','.') .'</td>';
                    $concat .= '</tr>';
                }
                echo $concat;
            ?>

        </table>
    </body>
</html>