<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit78a2e04ff75f404c7ee380ebb2e914c4
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Tercera\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Tercera\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit78a2e04ff75f404c7ee380ebb2e914c4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit78a2e04ff75f404c7ee380ebb2e914c4::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit78a2e04ff75f404c7ee380ebb2e914c4::$classMap;

        }, null, ClassLoader::class);
    }
}
