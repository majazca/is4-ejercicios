<?php
$data = [
        [
            'numero' => '9 x 1 = 9 ',
        ],
        [
            'numero' => '9 x 2 = 18 ',
        ],
         [
            'numero' => '9 x 3 = 27 ',
        ],
         [
            'numero' => '9 x 4 = 36 ',
        ],
         [
            'numero' => '9 x 5 = 45 ',
        ],
         [
            'numero' => '9 x 6 = 54 ',
        ],
         [
            'numero' => '9 x 7 = 63 ',
        ],
         [
            'numero' => '9 x 8 = 72 ',
        ],
         [
            'numero' => '9 x 9 = 81 ',
        ],
         [
            'numero' => '9 x 10 = 90 ',
        ],
    ];
?>

<!DOCTYPE html>
<html lang="en">
 	<head>
    <style>
   
    tr:nth-child(odd) { 
      background-color: #cdcdcd;
     
    }
	 table, td,th{
        border:1px solid black ;
        border-collapse:collapse;
        
      }
      
    </style>
    </head>
    <body>
       <table >
            <tr>
                <th  style="font-weight: normal; width= 100"> Tabla del 9</th>
               
            </tr>

            <?php

                $concat = '';

                foreach ($data as $tabla) {

 
                    $concat .= '<tr>';
                    $concat .= '<td>' . $tabla['numero'] .'</td>';
                    $concat .= '</tr>';
                }
                echo $concat;
            ?>

        </table>
    </body>
</html>
